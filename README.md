# Cyberdyne Backend Coding Challenge

Your objective is to create a basic version of an online course server functionality.

The point of this exercise is not to create a fully-functional bug-free server implementation,
but rather to look how you would design such a server system and how you approach different problems.

Feel free to use any technologies you feel most comfortable with in order to implement and deploy this server.
Use any free tools available to you.

The server should expose only REST endpoints using JSON as the format.

## Data structure

### Courses

- There are multiple **courses** (create a "ROS", "C++" and "Fortran" course in the database)
- Each **course** has multiple **lessons** (create 100 lessons for each course in the database)
- **Lessons** have a specific order in which they are displayed

### Users

- There are multiple **users** accessing the REST endpoints (e.g., from the Cyberlearn app)
- You don't have to create an authentication system, just hardcode any user-specific actions to a single user
- Each user can solve multiple **lessons**
- Each **lesson** can be solved multiple times
- Track the time the **user** starts the **lessons** and the time the **user** completes the **lesson**

### Achievements

- There are **achievements** for completing different objectives (see below)
- Each **user** can complete each of the achievements below
- Once a user completes an **achievement**, mark it as completed

#### Objectives

- Complete 5 lessons
- Complete 25 lessons
- Complete 50 lessons
- Complete the ROS course
- Complete the C++ course
- Complete the Fortran course

## API

### Courses and lessons list

Lessons and courses will be added or removed in the future, create an endpoint where the Cyberlearn app can 
request which courses and lessons are available. 

- Response contains only last start & completion time for each lesson

### Lesson progress

Create an endpoint where the Cyberlearn app can send information about lessons that the user completed.
The app will send the following data:

- Which lesson was completed
- When was the lesson started
- When was the lesson completed

### Achievements

Create an endpoint where the Cyberlearn app can request which achievements the users has completed
The app needs the following data:

- An identifier for the achievement
- If the achievement is completed or not
- What the progress of the achievement is (e.g. if the user solved three lessons, 
  it should return "3" for the lesson completion achievements)

## Further information

For the purpose of this challenge, ignore the contents of the course and consider that the network 
has enough performance to transfer the list of lessons or all achivements by a single request.
